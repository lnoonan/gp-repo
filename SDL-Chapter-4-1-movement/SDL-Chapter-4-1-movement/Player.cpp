#include <iostream>
#include "Player.h"

Player::Player(const LoaderParams* pParams) :SDLGameObject(pParams)
{
}

void Player::draw()
{
	SDLGameObject::draw(); // we now use SDLGameObject
}

void Player::update()
{


	m_currentFrame = int(((SDL_GetTicks() / 100) % 6));
  // m_acceleration.setX(-0.01); //page 77

	handleInput(); //every update we process the keyboard events detected by input handler


	SDLGameObject::update(); // we call this so that m_velocity is uodated by acceleration (if any)
							// and the position is updated by the velocity value
						   //we could omit this and set position in this function 
}

void Player::clean()
{
}

//we only use the keyboard in this example, we do not check bounds
void Player::handleInput()
{
if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_RIGHT))
	{
		m_velocity.setX(2);

	}
	if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_LEFT))
	{
		m_velocity.setX(-2);

	}
	if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_UP))
	{
		m_velocity.setY(-2);
	}
	if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_DOWN))
	{
	m_velocity.setY(2);
	}
	
}




