#ifndef CRITTER_H
#define CRITTER_H
#include <iostream>
using namespace std;

class Critter
{
public:          
    Critter(int hunger = 0, int boredom = 0, int age=0); 
    void Talk();
	void MyAgeIs();
    void Eat(int food = 4);
    void Play(int fun = 4);

private:
    int m_Hunger;
    int m_Boredom;
	int m_Age;

    int GetMood() const;
    void PassTime(int time = 1);
	int GetAge() const;

};


#endif


