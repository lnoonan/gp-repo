#ifndef __Game__
#define __Game__
#include "SDL.h"
#include "TextureManager.h"
#include "GameObject.h"
#include "Player.h"
#include "Enemy.h"
#include <iostream>
#include <vector>
class Game
{
private:
	SDL_Window* m_pWindow;
	SDL_Renderer* m_pRenderer;
	int m_currentFrame;
	
	GameObject* m_player;
	GameObject* m_player2;
	GameObject* m_enemy1;
	GameObject* m_enemy2;
	GameObject* m_enemy3;

	std::vector<GameObject*> m_gameObjects;

	bool m_bRunning;
public:
	Game() {}
	~Game() {}

	// simply set the running variable to true
	bool init(const char* title, int xpos, int ypos, int width,int height, bool fullscreen);

	void render();
	void update();
	void handleEvents();
	void clean();
	void draw();

	// a function to access the private running variable
	bool running() { return m_bRunning; }


};
#endif /* defined(__Game__) */

