#include <iostream>
#include "Player.h"

void Player::load(int x, int y, int width, int height, std::string textureID)
{
	GameObject::load(x, y, width, height, textureID);
}

void Player::draw(SDL_Renderer* pRenderer)
{
	GameObject::draw(pRenderer);
}

void Player::update()
{
	//using the fish sprite which has 5 frames
	m_x += 2;

m_currentFrame = int(((SDL_GetTicks() / 100) % 6));
}


