#include "Game.h"
#include "TextureManager.h"
#include <SDL_image.h>
#include <iostream>

bool Game::init(const char* title, int xpos, int ypos, int width,int height, bool fullscreen)
{
// attempt to initialize SDL
	if(SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{


		int flags = 0;
		if(fullscreen)
		{
			flags = SDL_WINDOW_FULLSCREEN;
		}
		std::cout << "SDL init success\n";
		// init the window
		m_pWindow = SDL_CreateWindow(title, xpos, ypos,width, height, flags);
		if(m_pWindow != 0) // window init success
		{
			std::cout << "window creation success about to setup textures\n";
			
			m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, 0);


			if(m_pRenderer != 0) // renderer init success
			{
												// to load
				if(!TextureManager::Instance()->load("assets/animate-alpha.png","animate", m_pRenderer))
				{
					return false;
				}

				if(!TextureManager::Instance()->load("assets/animate-fish-alpha-square.png","animate-fish", m_pRenderer))
				{
					return false;
				}
				std::cout << "renderer creation success\n";

				m_go.load(100, 100, 128, 82, "animate");
				m_player.load(300, 300, 43, 43, "animate-fish");

				SDL_SetRenderDrawColor(m_pRenderer,255,0,0,255);
			}
			else
			{
				std::cout << "renderer init fail\n";
				return false; // renderer init fail
			}
		}
		else
		{
			std::cout << "window init fail\n";
			return false; // window init fail
		}
	}
	else
	{
		std::cout << "SDL init fail\n";
		return false; // SDL init fail
	}
	std::cout << "init success\n";
	m_bRunning = true; // everything inited successfully, start the main loop
	return true;
}
void Game::render()
{
	SDL_RenderClear(m_pRenderer);

	//TheTextureManager::Instance()->drawFrame("animate", 100,100, 128, 82,1, m_currentFrame, m_pRenderer,SDL_FLIP_NONE);

	m_go.draw(m_pRenderer);  //chapter 3
	m_player.draw(m_pRenderer); //chapter 3


	SDL_RenderPresent(m_pRenderer);
}
void Game::clean()
{
	std::cout << "cleaning game\n";
	SDL_DestroyWindow(m_pWindow);
	SDL_DestroyRenderer(m_pRenderer);
	SDL_Quit();
}

void Game::update()
{
	m_go.update();
	m_player.update();
	//m_currentFrame = int(((SDL_GetTicks() / 100) % 6));
}

void Game::handleEvents()
{
	SDL_Event event;
	if(SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			m_bRunning = false;
		break;
		default:
		break;
		}
	}
}
