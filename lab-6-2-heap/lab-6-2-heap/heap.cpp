// Heap
// Demonstrates dynamically allocating memory

#include <iostream>

using namespace std;

int* intOnHeap();  //returns an int on the heap
void leak1();      //creates a memory leak
void leak2();      //creates another memory leak

int main()
{
    int* pHeap = new int;
    *pHeap = 10;
    cout << "*pHeap: " << *pHeap << "\n\n";
	cout << "pHeap contains the memloc " << pHeap << "\n\n";

    
    int* pHeap2 = intOnHeap();
    cout << "*pHeap2: which received pTemp " << *pHeap2 << "\n\n";
	cout << "pHeap2: memloc " << pHeap2 << "\n\n";
    cout << "Freeing memory pointed to by pHeap.\n\n";
    delete pHeap;

    cout << "Freeing memory pointed to by pHeap2.\n\n";
    delete pHeap2;
	cout << "pHeap2: memloc after free " << pHeap2 << "\n\n";
//	cout << "pHeap2: value referenced after free " << *pHeap2 << "\n\n";

	cout << "Freeing memory pointed to by pHeap.\n\n";
    //get rid of dangling pointers
    pHeap = 0; 
    pHeap2 = 0;
	leak2();
	getchar();
   
    return 0;
}

int* intOnHeap()
{
    int* pTemp = new int(20);
	cout << "*pTemp: " << *pTemp << "\n\n";
	cout << "pTemp contains the memloc " << pTemp << "\n\n";
    return pTemp;
}

void leak1()
{
    int* drip1 = new int(30);
	cout << "*drip1: " << *drip1 << "\n\n";
	cout << "drip1 contains the memloc " << drip1 << "\n\n";
	delete drip1;
	drip1 = 0;
}

void leak2()
{
    int* drip2 = new int(50);
	cout << "*drip2: " << *drip2 << "\n\n";
	cout << "drip2 contains the memloc " << drip2 << "\n\n";
	delete drip2;
	drip2 = 0;
    drip2 = new int(100);
	cout << "*drip2: " << *drip2 << "\n\n";
	cout << "drip2 contains the memloc " << drip2 << "\n\n";
    delete drip2;
}

