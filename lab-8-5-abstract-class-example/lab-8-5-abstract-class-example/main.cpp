#include "Orc.h"
#include "Troll.h"
#include <vector>
#include <list>


int main()
{
    Creature* pCreatureO = new Orc();
    pCreatureO->Greet();
    pCreatureO->DisplayHealth();
	pCreatureO->DisplaySpeed();
	Creature* pCreatureT = new Troll();
	pCreatureT->Greet();
	pCreatureT->DisplayHealth();
	pCreatureT->DisplaySpeed();

	vector<Creature*> vpCreatures; //created a vector of Creature pointers
	vpCreatures.push_back(pCreatureO); //added an Orc to the vector
	vpCreatures.push_back(pCreatureT);

	vector<Creature*>::const_iterator iter;
	cout << "Using a vector to cycle through all the Creatures \n";

	for (iter = vpCreatures.begin(); iter != vpCreatures.end(); ++iter)
	{
		(*iter)->Greet();
		(*iter)->DisplaySpeed();
		(*iter)->DisplayHealth();
	}

	list<Creature*> lpCreatures; //created a vector of Creature pointers
	lpCreatures.push_back(pCreatureO); //added an Orc to the vector
	lpCreatures.push_back(pCreatureT);

	list<Creature*>::const_iterator list_iter;
	cout << "Using a list to cycle through all the Creatures \n";

	for (list_iter = lpCreatures.begin(); list_iter != lpCreatures.end(); ++list_iter)
	{
		(*list_iter)->Greet();
		(*list_iter)->DisplaySpeed();
		(*list_iter)->DisplayHealth();
	}






	getchar();
    
    return 0;
}
