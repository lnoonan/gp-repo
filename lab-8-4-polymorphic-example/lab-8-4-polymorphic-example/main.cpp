#include "Boss.h"
#include <vector>

int main()
{
    cout << "Calling Attack() on Boss object through pointer to Enemy:\n";
    Enemy* pBadGuy = new Boss();
    pBadGuy->Attack();
	Enemy* pAnotherBadGuy = new Boss();
	pAnotherBadGuy->Attack();
	cout << "Vector of Enemy pointers created \n";
	vector<Enemy*> vEnemies;
	vEnemies.push_back(pBadGuy);
	vEnemies.push_back(pAnotherBadGuy);
	vEnemies[0]->Attack();
	vEnemies[1]->Attack();

   
    cout << "\n\nDeleting pointer to Enemy:\n";
    delete pBadGuy;
    pBadGuy = 0;
	getchar();
   
    return 0;
}
