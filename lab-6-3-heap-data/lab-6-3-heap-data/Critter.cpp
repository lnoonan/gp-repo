#include "Critter.h"
Critter::Critter(const string& name, int age)
{
	cout << "Constructor called for "<< name <<"\n";
	m_pName = new string(name);
	m_Age = age;
}

Critter::~Critter()                        //destructor definition
{
	cout << "Destructor called for the critter named " << *m_pName<<"\n";
	delete m_pName;
}

Critter::Critter(const Critter& c)        //copy constructor definition
{
	cout << "Copy Constructor called\n";
	m_pName = new string(*(c.m_pName));
	m_Age = c.m_Age;
}

Critter& Critter::operator=(const Critter& c)  //overloaded assignment op def
{
	cout << "Overloaded Assignment Operator called\n";
	if (this != &c)
	{
		delete m_pName;
		m_pName = new string(*(c.m_pName));
		m_Age = c.m_Age;
	}
	return *this;
}

void Critter::Greet() const
{
	cout << "I'm " << *m_pName << " and I'm " << m_Age << " years old. ";
	cout << "&m_pName: " << &m_pName << endl;
	cout << "m_pName points to the heap mem loc: " << m_pName << endl;
}