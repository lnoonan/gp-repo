//Heap Data Member
//Demonstrates an object with a dynamically allocated data member
#include "Critter.h"
void testDestructor();
void testCopyConstructor(Critter aCopy);
void testAssignmentOp();

int main()
{
	testDestructor();
	cout << endl;

	Critter crit("Poochie", 5);
	crit.Greet();
	testCopyConstructor(crit);
	crit.Greet();
	cout << endl;

	testAssignmentOp();

	return 0;
}

void testDestructor()
{
	Critter toDestroy("Rover", 3);
	toDestroy.Greet();
}

void testCopyConstructor(Critter aCopy)
{
	cout << "Test copy constructor \n";
	aCopy.Greet();
}

void testAssignmentOp()
{
	Critter crit1("crit1", 7);
	Critter crit2("crit2", 9);
	crit1 = crit2;
	cout << "about to call crit1.Greet() \n ";
	crit1.Greet();
	cout << "about to call crit2.Greet() \n ";
	crit2.Greet();
	cout << endl;

	Critter crit3("crit3", 11);
	crit3 = crit3;
	crit3.Greet();
	getchar();
}

